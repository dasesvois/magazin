import { createRouter, createWebHistory} from "vue-router";

import v_cart from "@/components/v_cart";
import v_catalog from "@/components/v_catalog";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'catalog',
            component: v_catalog
        },
        {
            path: '/cart',
            name: 'cart',
            component: v_cart,
            props: true
        }
    ]
})

export default router