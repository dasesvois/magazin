import {createStore} from 'vuex'
import axios from "axios";
import {jsonapiModule} from 'jsonapi-vuex'

const api = axios.create({
    baseURL: 'http://192.168.99.100',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    },
})


const store = createStore({
    modules: {
        jv: jsonapiModule(api),
    },
    state: {
        product: [],
        cart: [],
    },
    actions: {
        ADD_TO_CART({commit}, product) {
            commit('SET_CART', product)
        },
        DELETE_FROM_CART({commit}, product) {
            commit('REMOVE_FROM_CART', product)
        },
        INCREMENT_PRODUCT_COUNT_IN_CART({commit}, product) {
            commit('INCREMENT_PRODUCT_COUNT_IN_CART', product)
        },
        DECREMENT_PRODUCT_COUNT_IN_CART({commit}, product) {
            commit('DECREMENT_PRODUCT_COUNT_IN_CART', product)
        },
        ADD_CATALOG_IMG({commit}, product) {
            commit('GET_CATALOG_IMG', product)
        },
    },
    getters: {
        CART(state) {
            return state.cart
        },

        GET_PRODUCT_COUNT_IN_CART(state) {
            return function(product) {
                const idx = state.cart.findIndex(function (cartItem) {
                    return cartItem._jv.id === product._jv.id
                });

                if (state.cart[idx] === undefined) {
                    return 0
                }

                return state.cart[idx].count === undefined ? 0 : state.cart[idx].count
            }

        },

    },
    mutations: {
        SET_CART: (state, product) => {
            product.count = 1

            state.cart.push(product)
        },
        REMOVE_FROM_CART: (state, product) => {
            const idx = state.cart.findIndex(function (cartItem) {
                return cartItem._jv.id === product._jv.id
            });

            return state.cart.splice(idx, 1)
        },
        INCREMENT_PRODUCT_COUNT_IN_CART(state, product) {
            const idx = state.cart.findIndex(function (cartItem) {
                return cartItem._jv.id === product._jv.id
            });

            state.cart[idx].count += 1
        },
        DECREMENT_PRODUCT_COUNT_IN_CART(state, product) {
            const idx = state.cart.findIndex(function (cartItem) {
                return cartItem._jv.id === product._jv.id
            });

            state.cart[idx].count -= 1
        },
    }
});

export default store;