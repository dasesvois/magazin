import { createApp } from 'vue'
import App from './App.vue'
import './assets/main.css'
import VuexStore from './vuex/store'
import router from "@/router/router";

const app = createApp(App)
    app.use(VuexStore)
    app.use(router)
    app.mount('#app')
